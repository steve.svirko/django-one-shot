# Generated by Django 4.0.6 on 2022-07-27 01:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0003_todoitem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoitem',
            name='list',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='todos.todolist'),
        ),
    ]

from django.urls import include, path

from todos.views import (
    create_TodoItem,
    create_todo_list,
    show_model_name,
    todolist_detail,
    update_todo_list,
    delete_todo_list,
    update_todo_Item,
)

urlpatterns = [
    path("", show_model_name, name="todo_list_list"),
    path("<int:pk>/", todolist_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:pk>/edit/", update_todo_list, name="todo_list_update"),
    path("<int:pk>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_TodoItem, name="todo_item_create"),
    path("items/<int:pk>/edit/", update_todo_Item, name="todo_item_update"),
]

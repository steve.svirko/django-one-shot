from django.shortcuts import render, redirect
from todos.forms import (
    TodoListForm,
    UpdateTodoListForm,
    CreateTodoItemForm,
    UpdateTodoItemForm,
)

from todos.models import TodoList, TodoItem

# Create your views here.


def show_model_name(request):
    model_list = TodoList.objects.all()
    context = {"model_list": model_list}
    return render(request, "model_names/list.html", context)


def todolist_detail(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    context = {"model_instance": model_instance}
    return render(request, "model_names/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo_list = form.save()

            return redirect("todo_list_detail", new_todo_list.pk)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "model_names/create.html", context)


def update_todo_list(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        form = UpdateTodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            edited_todolist_name = form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("todo_list_detail", edited_todolist_name.pk)
    else:
        form = UpdateTodoListForm(instance=model_instance)

    context = {"form": form}

    return render(request, "model_names/edit.html", context)


def delete_todo_list(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "model_names/delete.html")


def create_TodoItem(request):
    if request.method == "POST":
        form = CreateTodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("todo_list_detail", pk=todoitem.list.id)
    else:
        form = CreateTodoItemForm()

    context = {"form": form}

    return render(request, "model_names/create_item.html", context)


# def update_todo_Item(request):
#     if request.method == "POST":
#         form = UpdateTodoItemForm(request.POST)
#         if form.is_valid():
#             todoitem = form.save()
#             # If you need to do something to the model before saving,
#             # you can get the instance by calling
#             # model_instance = form.save(commit=False)
#             # Modifying the model_instance
#             # and then calling model_instance.save()
#             return redirect("todo_list_detail", todoitem.list.id)
#     else:
#         form = UpdateTodoItemForm()

#     context = {"form": form}

#     return render(request, "model_names/edit_item.html", context)


def update_todo_Item(request, pk):
    if TodoItem and UpdateTodoItemForm:
        instance = TodoItem.objects.get(pk=pk)
        if request.method == "POST":
            form = UpdateTodoItemForm(request.POST, instance=instance)
            if form.is_valid():
                newtodoitem = form.save()
                return redirect("todo_list_detail", newtodoitem.list.id)
        else:
            form = UpdateTodoItemForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "model_names/edit_item.html", context)

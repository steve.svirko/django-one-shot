from django import forms

from todos.models import TodoItem, TodoList


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class UpdateTodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class CreateTodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        exclude = []

class UpdateTodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        exclude = []


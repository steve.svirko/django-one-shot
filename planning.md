FEATURE 8;



Create a view that shows the details of a particular to-do list, including its tasks




In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"




Create a template to show the details of the todolist and a table of its to-do items









Update the list template to show the number of to-do items for a to-do list



Update the list template to have a link from the to-do list name to the detail view for that to-do list